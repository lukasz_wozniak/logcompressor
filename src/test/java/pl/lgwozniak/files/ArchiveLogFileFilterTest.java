package pl.lgwozniak.files;

import org.junit.Assert;
import org.junit.Test;

import java.io.File;

/**
 * @author Łukasz Woźniak <a>http://lukasz-wozniak.pl</a> 30.11.13
 */
public class ArchiveLogFileFilterTest
{
    @Test
    public void testFileRegexFilter()
    {
        ArchiveLogFileFilter archiveFilter = new ArchiveLogFileFilter("D:\\Downloads\\compress-test");
        for (File file : archiveFilter.getDirectory().listFiles()) {
            if (!ArchiveLogFileFilter.matchFileName(file.getName())) {
                Assert.assertFalse("not passed" + file.getName(), false);
                System.out.println("not passed " + file.getName());
            } else {
                Assert.assertTrue("passed " + file.getName(), true);
                System.out.println("passed " + file.getName());
            }
        }

    }
}
