package pl.lgwozniak.files;

import com.google.common.base.Preconditions;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class filter the log file with the chosen extension
 * @author Łukasz Woźniak <a>http://lukasz-wozniak.pl</a> 30.11.13
 */
public class ArchiveLogFileFilter extends RegexFileFilter
{
    private static final Logger log =   LogManager.getLogger(ArchiveLogFileFilter.class);
    /** Pattern to search te archive log file */
    public static final Pattern NORMAL_DATE_PATTERN = Pattern.compile("((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])");

    private File directory = null;
    /** Filtered by the archive log files */
    Multimap<String, File> archiveFiles =  LinkedListMultimap.create();

    public ArchiveLogFileFilter(String directory)
    {
        super(NORMAL_DATE_PATTERN);
        this.directory = new File(directory);
        Preconditions.checkState(this.directory.isDirectory(), "%s is not a directory", directory);
    }

    /**
     * filter files
     */
    public void filter(){
       List<File> files = FileFilterUtils.filterList(this,directory.listFiles());
       log.info("archiveFiles: {}", files);
       for(File aFile : files){
           String[] fileName = StringUtils.split(aFile.getName(), ".");
           //not null and has more that 1 element
           if(fileName != null && fileName.length > 1){
                String ext = fileName[1];
               // @TODO check pattern is that what we want to split
               archiveFiles.put(parseFileExtension(ext), aFile);
           }
       }
    }

    /**
     * Here we now that this the date log. And we parse it to the
     * year-month type. We can cut last 3 characters
     * @param ext
     * @return
     */
    private String parseFileExtension(String ext) {
        if(ext.length() <= 3){
            return ext;
        } else {
            return ext.substring(0,ext.length()-3);
        }
    }

    /**
     * Checks to see if the part filename matches one of the regular expressions.
     *
     * @param dir   the file directory (ignored)
     * @param name  the filename
     * @return true if the filename matches one of the regular expressions
     */
    @Override
    public boolean accept(File dir, String name)
    {
        return matchFileName(name);
    }

    /**
     * Check if fileName match to ArchiveLogiFileFilter.NORMAL_DATE_PATTERN
     * @param fileName
     * @return
     */
    public static boolean matchFileName(String fileName){
        Matcher matcher = NORMAL_DATE_PATTERN.matcher(fileName);
        if(matcher.find())
            return true;
        else
            return false;
    }

    public File getDirectory()
    {
        return directory;
    }

    public Multimap<String, File> getArchiveFiles() {
        return archiveFiles;
    }
}
