package pl.lgwozniak.files.compress;

import com.google.common.collect.Lists;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.lgwozniak.files.ArchiveLogFileFilter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Class is giving methods compressing files to the zipped file,
 * and then remove files.
 *
 * @author Łukasz Woźniak <a>http://lukasz-wozniak.pl</a> 01.12.13
 */
public class CompressorFactory
{
    private static final Logger log = LogManager.getLogger(CompressorFactory.class);

    private boolean deleteFile;
    private ArchiveLogFileFilter logFilter;


    public CompressorFactory(ArchiveLogFileFilter logFilter, boolean deleteFile) {
        this.logFilter = logFilter;
        this.deleteFile = deleteFile;
    }

    /**
     * Main public class that compress fitlered files
     */
    public void compress(){
        List<File> fileToDelete = Lists.newArrayList();
        for(Map.Entry<String, Collection<File>> entry : logFilter.getArchiveFiles().asMap().entrySet()){
            Collection<File> files = entry.getValue();
            String ext = entry.getKey();

            compressFiles(ext, files);

            if(deleteFile)
                fileToDelete.addAll(files);
        }

        deleteFiles(fileToDelete);
    }

    /**
     * delete file if user want it
     * @param fileToDelete
     */
    private void deleteFiles(List<File> fileToDelete) {
        if(CollectionUtils.isNotEmpty(fileToDelete))
            fileToDelete.stream().forEach((f) -> FileUtils.deleteQuietly(f));
    }

    /**
     * Compress group files
     * @param extName
     * @param files
     */
    private void compressFiles(String extName, Collection<File> files){
        StringBuilder sb = new StringBuilder(File.separator)
                .append("log").append('-').append(extName).append("archive.zip");

        File archiveFile = new File(logFilter.getDirectory() + sb.toString());
        log.info("Compressing the file ....");
        log.info("Compressed file : {} ", archiveFile);


        ZipArchiveOutputStream os = null;
        try {
            os = (ZipArchiveOutputStream) new ArchiveStreamFactory().createArchiveOutputStream("zip", new FileOutputStream(archiveFile));
            writeFiles(os, files);
            os.flush();
        }catch(IOException | ArchiveException e){
            e.printStackTrace();
        }
        finally{
            IOUtils.closeQuietly(os);
        }
    }

    /**
     * Save content of the file to the compressed archive
     * @param os
     * @throws IOException
     */
    private static void writeFiles(ZipArchiveOutputStream os, Collection<File> files) throws IOException
    {
        for(File file : files){
            ZipArchiveEntry entry = new ZipArchiveEntry(file, file.getName() );
            os.putArchiveEntry(entry);
            os.write(IOUtils.toByteArray(new FileInputStream(file)));
        }
        os.closeArchiveEntry();
    }

}
