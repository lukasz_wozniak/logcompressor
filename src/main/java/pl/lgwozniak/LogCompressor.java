package pl.lgwozniak;

import org.apache.commons.lang3.StringUtils;
import pl.lgwozniak.files.ArchiveLogFileFilter;
import pl.lgwozniak.files.compress.CompressorFactory;

import java.io.File;

/**
 * Main execution class
 * @author Łukasz Woźniak <a>http://lukasz-wozniak.pl</a> 30.11.13
 */
public class LogCompressor
{

    public static void main(String[] args)
    {
        if(args == null || args.length == 0){
            throw new IllegalArgumentException("First argument is directory with log files, second is to delete files or not.");
        }

        if(!(new File(args[0]).isDirectory()))
        {
            throw new IllegalArgumentException("First argument is directory is not a directory");
        }

        boolean deleteFile = args.length == 2 && args[1] != null ? Boolean.valueOf(args[1]) : false;

        try {
            ArchiveLogFileFilter archiveFilter = new ArchiveLogFileFilter(args[0]);
            archiveFilter.filter();
            new CompressorFactory(archiveFilter, deleteFile).compress();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
